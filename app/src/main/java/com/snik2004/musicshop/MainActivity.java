package com.snik2004.musicshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Button btnPlus;
    Button btnMinus;
    Button btnAddToCart;
    Button btnGoToCart;
    TextView textViewCount;
    EditText userNameEditText;
    int quantity = 0;
    List<String> list = new ArrayList<>();
    Spinner spinner;
    ArrayAdapter arrayAdapter;
    Map<String, Integer> goodsMap = new HashMap();
    String goodsName;
    double orderPrice;
    Order order = new Order();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initButton();
        addToMapList();
        createSpinner();


        textViewCount = findViewById(R.id.text_view_count);
        userNameEditText = findViewById(R.id.user_name);
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantity = quantity + 1;
                String string = String.valueOf(quantity);
                textViewCount.setText(string);
                TextView priceTextView = findViewById(R.id.price_tv);
                String string2 = String.valueOf(quantity * orderPrice);
                priceTextView.setText(string2);
            }
        });
        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quantity != 0) {
                    quantity = quantity - 1;
                }
                String string = String.valueOf(quantity);
                textViewCount.setText(string);
                TextView priceTextView = findViewById(R.id.price_tv);
                String string2 = String.valueOf(quantity * orderPrice);
                priceTextView.setText(string2);
            }
        });
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart();
            }
        });
        btnGoToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCart();
            }
        });
    }

    void initButton() {
        btnPlus = findViewById(R.id.button_plus);
        btnMinus = findViewById(R.id.button_minus);
        btnAddToCart = findViewById(R.id.button_add_to_cart);
        btnGoToCart = findViewById(R.id.button_go_to_cart);
    }

    void createSpinner() {
        spinner = findViewById(R.id.spinner);
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(this);
    }

    void addToMapList() {
        list.add("lol");
        list.add("angry");
        list.add("troll");
        list.add("fuuu");

        goodsMap.put("lol", 350);
        goodsMap.put("angry", 1140);
        goodsMap.put("troll", 2030);
        goodsMap.put("fuuu", 135);

    }

    void addToCart() {
        order.userName = userNameEditText.getText().toString();
        Log.d("userName: ", order.userName.toString());
        order.goodsName=goodsName;
        Log.d("goodsName: ", order.goodsName.toString());
        order.quantity=quantity;
        Log.d("quantity: ", String.valueOf(order.quantity));
        order.orderPrice= orderPrice;
        Log.d("orderPrice: ", String.valueOf(order.orderPrice));
    }

    void goToCart(){
        Intent orderIntent = new Intent( MainActivity.this, OrderActivity.class);
        orderIntent.putExtra("orderUserName", order.userName);
        orderIntent.putExtra("orderGoodsName", order.goodsName);
        orderIntent.putExtra("orderQuantity", order.quantity);
        orderIntent.putExtra("orderPrice", order.orderPrice);
        startActivity(orderIntent);
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        goodsName = spinner.getSelectedItem().toString();
        orderPrice = (double) goodsMap.get(goodsName);
        TextView priceTextView = findViewById(R.id.price_tv);
        String string = String.valueOf(quantity * orderPrice);
        priceTextView.setText(string);

        ImageView goodsImageView = findViewById(R.id.goods_image_view);
        switch (goodsName) {
            case "angry":
                goodsImageView.setImageResource(R.drawable.angry);
                break;
            case "fuuu":
                goodsImageView.setImageResource(R.drawable.fuuu);
                break;
            case "lol":
                goodsImageView.setImageResource(R.drawable.lol);
                break;
            default:
                goodsImageView.setImageResource(R.drawable.troll);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
