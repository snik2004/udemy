package com.snik2004.musicshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class OrderActivity extends AppCompatActivity {
    TextView orderUserNameTv;
    TextView orderGoodsNameTv;
    TextView orderQuantityTv;
    TextView orderPriceTv;
    TextView priceTv;
    Button sendEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        setTitle(R.string.order_title);
        initViewAndButton();
        Intent orderIntent = getIntent();
        String userName = orderIntent.getStringExtra("orderUserName");
        String orderGoodsName = orderIntent.getStringExtra("orderGoodsName");
        int orderQuantity = orderIntent.getIntExtra("orderQuantity", 0);
        double orderPrice = orderIntent.getDoubleExtra("orderPrice", 0);
        orderUserNameTv.setText("Customer name: " + userName);
        orderGoodsNameTv.setText("Goods name: " + orderGoodsName);
        priceTv.setText("Price: " + String.valueOf(orderPrice));
        orderQuantityTv.setText("Quantity: "+String.valueOf(orderQuantity));
        orderPriceTv.setText("Order price: "+String.valueOf(orderPrice*orderQuantity));
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("Customer name: ").append(userName).append("\n")
                .append("goods name: ").append(orderGoodsName).append("\n")
                .append("price: " ).append(orderPrice).append("\n")
                .append("quantity: ").append(orderQuantity).append("\n")
                .append("order price: ").append(orderPrice*orderQuantity);
        sendEmail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                composeEmail(stringBuilder.toString());
            }
        });


    }
    public void composeEmail(String subject) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        //intent.setData(Uri.parse("mailto:"));
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_TEXT, subject);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Заказ с приложения");
        if (intent.resolveActivity(getPackageManager()) != null) {
            if (intent==null){
                Toast email_not_send = Toast.makeText(this, "Email not send",Toast.LENGTH_LONG);
                email_not_send.show();
            }
            startActivity(intent);
        }
    }
    void initViewAndButton() {
        sendEmail = findViewById(R.id.send_email);
        orderUserNameTv = findViewById(R.id.order_user_name);
        orderGoodsNameTv = findViewById(R.id.order_goods_name);
        orderQuantityTv = findViewById(R.id.order_quantity);
        orderPriceTv = findViewById(R.id.order_price);
        priceTv = findViewById(R.id.price);

    }
}
